const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email : reqBody.email}).then(result => {
		if (result.length > 0) {
			return true;
		} else {
			return false;
		};
	});
};

module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

module.exports.loginUser = (reqBody) => {
	
	// We use the "findOne" method instead of the "find" method which return all records that match the search criteria
	// The "findOne" method returns the first record in the collection that matches the search criteria
	return User.findOne({email : reqBody.email}).then(result => {
		
		// User does not exist
		if (result == null) {
			return false;
		
		// User exist
		} else {
			
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieve from the database and it returns "true" or "false" value depending on the result.
			// A good coding practice for boolean variable/constants is to use the work "is" or "are" at the beginning in the form of is+Noun
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				
				// Generate an access token
				return { access : auth.createAccessToken(result)}
			} else {
				return false;
			};
		};
	});
};

module.exports.getProfile = (data) => {
	return User.findOne(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user.
module.exports.enroll = async (data) => {
	// "async" is placed in line 74 to declare that is an async expression
	// Given that it was an async expression, "await" keyword should be permitted within the body


	// An asyn expression, is an expression that returns a promise.

	let isUserUpdated = await User.findById(data.userId).then(user => {
		// await is written above which tells our code to wait for the promise to resolve, in this case for our user enrollments to be "push" in our database.


		// Adds the courseId in the users's enrollments array
		user.enrollments.push({courseId : data.courseId});

		// Saves the updated user information in the database
		return user.save().then((user, error) =>{
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.userId});

		return course.save().then((course, error) =>{
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});

	// To check if we are successful in fulfilling both promise, we will utilize the if statement and the double ampersand as representation of logical "AND"
	if (isUserUpdated && isCourseUpdated) {
		return true;
	} else {
		return false;
	};
};


module.exports.enroll = async (data) => {

/*	let isUserUpdated = await User.findById(data.isAdmin).then(user => {
		// await is written above which tells our code to wait for the promise to resolve, in this case for our user enrollments to be "push" in our database.


		// Adds the courseId in the users's enrollments array
		user.enrollments.push({courseId : data.courseId});

		// Saves the updated user information in the database
		return user.save().then((user, error) =>{
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});*/
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {

		course.enrollees.push({userId : data.isAdmin});

		return course.save().then((course, error) =>{
			if (error) {
				return false;
			} else {
				return true;
			};
		});
	});
	if (isCourseUpdated) {
		return true;
	} else {
		false;
	}
}